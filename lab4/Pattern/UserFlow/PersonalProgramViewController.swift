//
//  PersonalProgramViewController.swift
//  Pattern
//
//  Created by Martina on 14.05.2020.
//  Copyright © 2020 MartinaRogach. All rights reserved.
//

import UIKit

class PersonalProgramViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var previousButton: UIButton!
    
    var program = PersonalProgramModel()
    var undoStack: UndoStack!
    override func viewDidLoad() {
        super.viewDidLoad()

        let builder = ProgramBuilder()
        let director = PersonalProgramDirector()
        director.update(builder: builder)
        director.buildSimpleProgram()
        undoStack = UndoStack(program)
        program.activities = builder.getProgram()
        undoStack.save()
        print(self.undoStack.printAll())
        tableView.register(UINib(nibName: "\(PersonalProgramTableViewCell.self)", bundle: nil), forCellReuseIdentifier: "\(PersonalProgramTableViewCell.self)")
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return program.activities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "\(PersonalProgramTableViewCell.self)", for: indexPath) as? PersonalProgramTableViewCell else { return UITableViewCell() }
        cell.configure(with: program.activities[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    
    @IBAction func anotherPpButtonPressed(_ sender: Any) {
        let alert = UIAlertController(title: "Which program would you like to try?", message: nil, preferredStyle: .actionSheet)
        let action = UIAlertAction(title: "Full Personal Program", style: .default) { (_) in
            let builder = ProgramBuilder()
            let director = PersonalProgramDirector()
            director.update(builder: builder)
            director.buildFullProgram()
            self.program.activities = builder.getProgram()
            self.undoStack.save()
            self.previousButton.isEnabled = true
            print(self.undoStack.printAll())
            self.tableView.reloadData()
        }
        let secondAction = UIAlertAction(title: "Dance Personal Program", style: .default) { (_) in
            let builder = ProgramBuilder()
            let director = PersonalProgramDirector()
            director.update(builder: builder)
            director.buildThirdProgram()
            self.program.activities = builder.getProgram()
            self.undoStack.save()
            self.previousButton.isEnabled = true
            self.tableView.reloadData()
        }
        let cancel = UIAlertAction(title: "cancel", style: .cancel, handler: nil)
        alert.addAction(action)
        alert.addAction(secondAction)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func previousButtonPressed(_ sender: Any) {
        if !undoStack.isEmpty() {
            undoStack.undo()
            undoStack.printAll()
            tableView.reloadData()
            if undoStack.isEmpty() {
                previousButton.isEnabled = false
            }
        }
    }
}
