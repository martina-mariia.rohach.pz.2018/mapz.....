//
//  BookObserver.swift
//  Pattern
//
//  Created by Martina on 25.05.2020.
//  Copyright © 2020 MartinaRogach. All rights reserved.
//

import UIKit

//OBSERVER
protocol BookSubscriber {
    func accept(changed workout: ScheduleItem?)
}

class BookSubscriberManager {

    private var workout: ScheduleItem?
    private lazy var subscribers = [BookSubscriber]()

    func add(subscriber: BookSubscriber) {
        print("BookSubscriberManager: I'am adding a new subscriber")
        subscribers.append(subscriber)
    }

    func add(workout: ScheduleItem) {
        print("\nBookSubscriberManager: I'am adding a new workout")
        self.workout = workout
        notifySubscribers()
    }

    func remove(subscriber filter: (BookSubscriber) -> (Bool)) {
        guard let index = subscribers.firstIndex(where: filter) else { return }
        subscribers.remove(at: index)
    }

    func remove() {
        if !(workout == nil) {
            workout = nil
            notifySubscribers()
        }
    }

    private func notifySubscribers() {
        subscribers.forEach({ $0.accept(changed: workout) })
    }
}

extension UINavigationController: BookSubscriber {
    func accept(changed workout: ScheduleItem?) {
        if let work = workout {
            navigationBar.topItem?.title = "You've booked \(work.title)"
        } else {
            navigationBar.topItem?.title = "No workout booked"
        }
        print("UINavigationController: Updating an appearance of navigation items")
    }
}

