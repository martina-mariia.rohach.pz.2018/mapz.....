//
//  AddStrategy.swift
//  Pattern
//
//  Created by Martina on 24.05.2020.
//  Copyright © 2020 MartinaRogach. All rights reserved.
//

import UIKit
import RealmSwift

protocol SendComplete: UIViewController {
    func update()
}

protocol Strategy {
    var strategyController: SendComplete { get set }
    func presentAlert()
}

class NoMembershipStrategy: Strategy {
    
    var strategyController: SendComplete
    
    init(vc: SendComplete) {
        self.strategyController = vc
    }
    
    func presentAlert() {
        let alert = UIAlertController(title: "You have no membership!", message: "Would you like to try?", preferredStyle: .alert)
        let action = UIAlertAction(title: "Yes", style: .destructive) { (_) in
            let buyAlert = UIAlertController(title: "Which membership would you like to try?", message: nil, preferredStyle: .actionSheet)
            let action = UIAlertAction(title: "With 8 trainings", style: .default) { (_) in
                let realm = try? Realm()
                let membership = MembershipModel()
                membership.title = "Simple Membership"
                membership.numberOfTimes = 8
                membership.useTimes = 2
                try? realm?.write {
                    realm?.add(membership)
                }
                self.strategyController.update()
            }
            let secondAction = UIAlertAction(title: "With 12 trainings", style: .default) { (_) in
                let realm = try? Realm()
                let membership = MembershipModel()
                membership.title = "Pro Membership"
                membership.numberOfTimes = 12
                membership.useTimes = 2
                try? realm?.write {
                    realm?.add(membership)
                }
                self.strategyController.update()
            }
            let cancelAction = UIAlertAction(title: "No, thanks", style: .cancel, handler: nil)
            buyAlert.addAction(action)
            buyAlert.addAction(secondAction)
            buyAlert.addAction(cancelAction)
            self.strategyController.present(buyAlert, animated: true, completion: nil)
        }
        let secondAction = UIAlertAction(title: "No, thanks", style: .cancel, handler: nil)
        alert.addAction(action)
        alert.addAction(secondAction)
        self.strategyController.present(alert, animated: true, completion: nil)
    }
}

class GetPPStrategy: Strategy {
    
    var strategyController: SendComplete
    
    init(vc: SendComplete) {
        self.strategyController = vc
    }
    
    func presentAlert() {
        let alert = UIAlertController(title: "You can upgrade Your Personal Program!", message: "Would you like to redirect to PP page?", preferredStyle: .alert)
        let action = UIAlertAction(title: "Yes", style: .destructive) { (_) in
            self.strategyController.tabBarController?.selectedIndex = 1
        }
        let secondAction = UIAlertAction(title: "No, thanks", style: .cancel, handler: nil)
        alert.addAction(action)
        alert.addAction(secondAction)
        self.strategyController.present(alert, animated: true, completion: nil)
    }
}
