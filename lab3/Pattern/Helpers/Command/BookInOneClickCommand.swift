//
//  BookInOneClickCommand.swift
//  Pattern
//
//  Created by Martina on 25.05.2020.
//  Copyright © 2020 MartinaRogach. All rights reserved.
//

import Foundation
import RealmSwift

protocol Command {
    func execute()
}

class BookInOneClickCommand: Command {
    let availability = Availability()
    
    func execute() {
        let realm = try? Realm()
        if realm?.objects(Availability.self).count == 0 {
            let avail = Availability()
            avail.numberOfPeople = 0
            avail.available = true
            try? realm?.write {
                realm?.add(avail)
            }
        }
        guard let object = realm?.objects(Availability.self).first else { return }
        if object.available {
            WorkoutBooker.shared.bookWorkout()
        }
    }
}
