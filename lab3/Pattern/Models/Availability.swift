//
//  Availability.swift
//  Pattern
//
//  Created by Martina on 14.05.2020.
//  Copyright © 2020 MartinaRogach. All rights reserved.
//

import Foundation
import RealmSwift

//FACADE
class Availability: Object {
    @objc dynamic var numberOfPeople: Int = 0
    @objc dynamic var available: Bool = true
}
