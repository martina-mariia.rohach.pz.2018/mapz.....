//
//  ActivityModel.swift
//  Pattern
//
//  Created by Martina on 13.05.2020.
//  Copyright © 2020 MartinaRogach. All rights reserved.
//

import Foundation

//BUILDER
class Activity: Codable {
    var title: String
    var duration: Int
    var image: String
    
    enum CodingKeys: String, CodingKey {
        case title, duration, image
    }
    
    required init(from decoder: Decoder) {
        let container = try! decoder.container(keyedBy: CodingKeys.self)
        do {
            title = try container.decode(String.self, forKey: .title)
        } catch {
            title = "Fitness"
        }
        do {
            duration = try container.decode(Int.self, forKey: .duration)
        } catch {
            duration = 0
        }
        do {
            image = try container.decode(String.self, forKey: .image)
        } catch {
            image = ""
        }
    }
}

class PersonalProgramModel: Codable {
    var activities: [Activity]
    
    enum CodingKeys: String, CodingKey {
        case activities
    }
    
    init() {
        activities = []
    }
    
    func getSimpleProgram() {
        Server.shared.doRequest(url: "SimplePersonalProgram") { (result: Result<PersonalProgramModel, Error>) in
            switch result {
            case .success(let activities):
                self.activities.append(contentsOf: activities.activities)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getFullProgram() {
        Server.shared.doRequest(url: "FullPersonalProgram") { (result: Result<PersonalProgramModel, Error>) in
            switch result {
            case .success(let activities):
                self.activities.append(contentsOf: activities.activities)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getThirdProgram() {
        Server.shared.doRequest(url: "ThirdPersonalProgram") { (result: Result<PersonalProgramModel, Error>) in
            switch result {
            case .success(let activities):
                self.activities.append(contentsOf: activities.activities)
            case .failure(let error):
                print(error)
            }
        }
    }
}
