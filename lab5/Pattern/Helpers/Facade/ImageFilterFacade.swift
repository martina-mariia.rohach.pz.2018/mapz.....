//
//  ImageFilterFacade.swift
//  Pattern
//
//  Created by Martina on 14.05.2020.
//  Copyright © 2020 MartinaRogach. All rights reserved.
//

import Foundation
import RealmSwift

//FACADE
class ImageFilterFacade {
    
    func applyTo(image: UIImage) {
        let image = UIImage(named: "stretching")
        let resizer = Resizer(image as! ImageEditor, xScale: 0.2, yScale: 0.2)

        let blurFilter = BlurFilter(resizer)
        blurFilter.update(radius: 2)

        let colorFilter = ColorFilter(blurFilter)
        colorFilter.update(contrast: 0.53)
        colorFilter.update(brightness: 0.12)
        colorFilter.update(saturation: 4)
        clientCode(editor: colorFilter)
    }
    
    func clientCode(editor: ImageEditor) {
        let image = editor.apply()
        print("Client: all changes have been applied for \(image)")
    }
}
