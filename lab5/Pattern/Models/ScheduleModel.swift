//
//  ScheduleModel.swift
//  Pattern
//
//  Created by Martina on 13.05.2020.
//  Copyright © 2020 MartinaRogach. All rights reserved.
//

import Foundation


//ABSTRACT FACTORY
protocol Schedule {
    var items: [ScheduleItem] { get set }
    func getFromJson()
}

class ScheduleItem: Codable {
    var title: String
    var day: String
    var time: String
    
    enum CodingKeys: String, CodingKey {
        case title, day, time
    }
    
    required init(from decoder: Decoder) {
        let container = try! decoder.container(keyedBy: CodingKeys.self)
        do {
            title = try container.decode(String.self, forKey: .title)
        } catch {
            title = "Fitness"
        }
        do {
            day = try container.decode(String.self, forKey: .day)
        } catch {
            day = "Sunday"
        }
        do {
            time = try container.decode(String.self, forKey: .time)
        } catch {
            time = ""
        }
    }
}

class CoachSchedule: Schedule, Codable {
    var items: [ScheduleItem]
    
    enum CodingKeys: String, CodingKey {
        case items
    }
    
    init() {
        items = []
    }
    
    func getFromJson() {
        Server.shared.doRequest(url: "CoachSchedule") { (result: Result<CoachSchedule, Error>) in
            switch result {
            case .success(let schedule):
                self.items = schedule.items
            case .failure(let error):
                print(error)
            }
        }
    }
}

class TraineeSchedule: Schedule, Codable {
    var items: [ScheduleItem]
    
    enum CodingKeys: String, CodingKey {
        case items
    }
    
    init() {
        items = []
    }
    
    func getFromJson() {
        Server.shared.doRequest(url: "TraineeSchedule") { (result: Result<TraineeSchedule, Error>) in
            switch result {
            case .success(let schedule):
                self.items = schedule.items
            case .failure(let error):
                print(error)
            }
        }
    }
}
