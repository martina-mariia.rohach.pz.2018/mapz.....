//
//  WorkoutBooker.swift
//  Pattern
//
//  Created by Martina on 14.05.2020.
//  Copyright © 2020 MartinaRogach. All rights reserved.
//

import Foundation
import RealmSwift

//FACADE
class WorkoutBooker {
    static var shared = WorkoutBooker() //SINGLETON
    
    private init() {}
    
    func bookWorkout() {
        let realm = try? Realm()
        guard let object = realm?.objects(Availability.self).first else { return }
        try? realm?.write {
            object.numberOfPeople += 1
        }
        if object.numberOfPeople >= 15 {
            try? realm?.write {
                object.available = false
            }
        }
    }
    
    func removeBooking() {
        let realm = try? Realm()
        guard let object = realm?.objects(Availability.self).first else { return }
        try? realm?.write {
            object.numberOfPeople -= 1
        }
        if object.numberOfPeople >= 15 {
            try? realm?.write {
                object.available = true
            }
        }
    }
}
