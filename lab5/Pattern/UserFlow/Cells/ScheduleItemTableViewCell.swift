//
//  ScheduleItemTableViewCell.swift
//  Pattern
//
//  Created by Martina on 14.05.2020.
//  Copyright © 2020 MartinaRogach. All rights reserved.
//

import UIKit

class ScheduleItemTableViewCell: UITableViewCell {

    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configure(with item: ScheduleItem) {
        dayLabel.text = item.day
        titleLabel.text = item.title
        timeLabel.text = item.time
    }
}
