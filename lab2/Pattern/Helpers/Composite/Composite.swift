//
//  Composite.swift
//  Pattern
//
//  Created by Martina on 14.05.2020.
//  Copyright © 2020 MartinaRogach. All rights reserved.
//

import UIKit

//COMPOSITE
protocol Component {
    func accept<T: Theme>(theme: T)
}

extension Component where Self: UIViewController {
    func accept<T: Theme>(theme: T) {
        view.accept(theme: theme)
        view.subviews.forEach({ $0.accept(theme: theme) })
    }
}

extension UIView: Component {}
extension UIViewController: Component {}

extension Component where Self: UIView {
    func accept<T: Theme>(theme: T) {
        backgroundColor = theme.backgroundColor
    }
}

extension Component where Self: UILabel {
    func accept<T: LabelTheme>(theme: T) {
        backgroundColor = theme.backgroundColor
        textColor = theme.textColor
    }
}

extension Component where Self: UIButton {
    func accept<T: ButtonTheme>(theme: T) {
        backgroundColor = theme.backgroundColor
        setTitleColor(theme.textColor, for: .normal)
        setTitleColor(theme.highlightedColor, for: .highlighted)
    }
}

protocol Theme {
    var backgroundColor: UIColor { get }
}

protocol ButtonTheme: Theme {
    var textColor: UIColor { get }
    var highlightedColor: UIColor { get }
}

protocol LabelTheme: Theme {
    var textColor: UIColor { get }
}


struct DefaultButtonTheme: ButtonTheme {
    var textColor = UIColor.red
    var highlightedColor = UIColor.white
    var backgroundColor = UIColor.orange
}

struct NightButtonTheme: ButtonTheme {
    var textColor = UIColor.white
    var highlightedColor = UIColor.red
    var backgroundColor = UIColor.black
}


struct DefaultLabelTheme: LabelTheme {
    var textColor = UIColor.red
    var backgroundColor = UIColor.black
}

struct NightLabelTheme: LabelTheme {
    var textColor = UIColor.white
    var backgroundColor = UIColor.black
}

