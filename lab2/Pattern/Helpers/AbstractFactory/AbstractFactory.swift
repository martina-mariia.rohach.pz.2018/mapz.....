//
//  AbstractFactory.swift
//  Pattern
//
//  Created by Martina on 13.05.2020.
//  Copyright © 2020 MartinaRogach. All rights reserved.
//

import Foundation

//ABSTRACT FACTORY
protocol AbstractFactory {
    func createSchedule() -> Schedule
}

class TraineeScheduleFactory: AbstractFactory {
    func createSchedule() -> Schedule {
        let schedule = TraineeSchedule()
        schedule.getFromJson()
        return schedule
    }
}

class CoachScheduleFactory: AbstractFactory {
    func createSchedule() -> Schedule {
        let schedule = CoachSchedule()
        schedule.getFromJson()
        return schedule
    }
}
