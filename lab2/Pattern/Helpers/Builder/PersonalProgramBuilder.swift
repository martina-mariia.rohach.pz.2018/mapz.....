//
//  PersonalProgramBuilder.swift
//  Pattern
//
//  Created by Martina on 13.05.2020.
//  Copyright © 2020 MartinaRogach. All rights reserved.
//

import Foundation

//BUILDER
protocol Builder {
    func produceSimpleProgram()
    func produceFullProgram()
    func produceThirdProgram()
}

class ProgramBuilder: Builder {
    
    private var program = PersonalProgramModel()
    
    func produceSimpleProgram() {
        program.getSimpleProgram()
    }
    
    func produceFullProgram() {
        program.getFullProgram()
    }
    
    func produceThirdProgram() {
        program.getThirdProgram()
    }
    
    func getProgram() -> [Activity] {
        return self.program.activities
    }
}
