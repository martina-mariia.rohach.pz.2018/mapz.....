//
//  PersonalProgramDirector.swift
//  Pattern
//
//  Created by Martina on 13.05.2020.
//  Copyright © 2020 MartinaRogach. All rights reserved.
//

import Foundation

class PersonalProgramDirector {

    private var builder: Builder?

    func update(builder: Builder) {
        self.builder = builder
    }

    func buildSimpleProgram() {
        builder?.produceSimpleProgram()
    }

    func buildFullProgram() {
        builder?.produceSimpleProgram()
        builder?.produceFullProgram()
    }
    
    func buildThirdProgram() {
        builder?.produceSimpleProgram()
        builder?.produceThirdProgram()
    }
}
