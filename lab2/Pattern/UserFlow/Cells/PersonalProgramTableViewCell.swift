//
//  PersonalProgramTableViewCell.swift
//  Pattern
//
//  Created by Martina on 14.05.2020.
//  Copyright © 2020 MartinaRogach. All rights reserved.
//

import UIKit

class PersonalProgramTableViewCell: UITableViewCell {

    @IBOutlet weak var workoutImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(with prgram: Activity) {
        titleLabel.text = prgram.title
        durationLabel.text = String(prgram.duration)
        workoutImageView.image = UIImage(named: prgram.image)
    }
}
