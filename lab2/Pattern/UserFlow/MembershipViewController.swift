//
//  MembershipViewController.swift
//  Pattern
//
//  Created by Martina on 24.05.2020.
//  Copyright © 2020 MartinaRogach. All rights reserved.
//

import UIKit
import RealmSwift

class MembershipViewController: UIViewController, SendComplete {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var progress: UIProgressView!
    @IBOutlet weak var usage: UILabel!
    @IBOutlet weak var buyNewButton: UIButton!
    @IBOutlet weak var useTimesLabel: UILabel!
    
    var membershipModel: MembershipModel?
    var strategy: Strategy?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.image = UIImage(named: "yoga")
        let realm = try? Realm()
        if let object = realm?.objects(MembershipModel.self).first {
            membershipModel = object
            update()
        } else {
            strategy = NoMembershipStrategy(vc: self)
            strategy?.presentAlert()
        }
        
    }

    func update() {
        let realm = try? Realm()
        membershipModel = realm?.objects(MembershipModel.self).first
        titleLabel.text = membershipModel?.title
        usage.text = "This membership include:\nYoga\nZumba\nBody Shape"
        progress.setProgress(Float(Float(membershipModel!.useTimes) / Float(membershipModel!.numberOfTimes)), animated: true)
        useTimesLabel.text = "You use \(membershipModel!.useTimes)/\(membershipModel!.numberOfTimes) times"
        buyNewButton.isEnabled = true
    }
}
